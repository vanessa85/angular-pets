import { Component, OnInit } from '@angular/core';

import { PetService } from '../pet.service';
import { Pet } from '../models/pet';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.css']
})
export class PetsComponent implements OnInit {
  pets: Pet[];

  constructor(private petService: PetService) { }

  ngOnInit() {
    this.getPets();
  }

  getPets(): void {
    this.pets = this.petService.getPets();
  }

  removePet(event, pet) {
    event.preventDefault();
    if(confirm("Are you sure?")) {
      this.petService.removePet(pet);
    }

  }

}
