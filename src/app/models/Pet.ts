import { Category } from './category';

export class Pet {
  id: number;
  category: Category;
  name: string;
  photoUrls: string[];
  tags: string[];
}