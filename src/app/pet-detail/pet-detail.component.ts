import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { PetService } from '../pet.service';
import { CategoryService } from '../category.service';
import { Pet } from '../models/pet';
import { Category } from '../models/category';

@Component({
  selector: 'app-pet-detail',
  templateUrl: './pet-detail.component.html',
  styleUrls: ['./pet-detail.component.css']
})
export class PetDetailComponent implements OnInit {
  pet: Pet = {
    id: 0,
    name: '',
    category: { id: 0, name: '' },
    photoUrls: [],
    tags: []
  };

  categories: Category[];

  action: string;

  constructor(
    private route: ActivatedRoute, 
    private petService: PetService,
    private categoryService: CategoryService,
    private location: Location) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.action  = id? 'edit' : 'add'; 

    if (this.action === 'edit') {
      this.getPet(id);
    }

    this.getCategories();
  }

  getPet(id) {
    this.pet = this.petService.getPet(id);
  }

  getCategories() {
    this.categories = this.categoryService.getCategories();
  }

  goBack(): void {
    this.location.back();
  }

  save(event) {
    if (this.action === 'add') {
      this.petService.addPet(this.pet);
    } else {
      this.petService.updatePet(this.pet);
    }
    this.location.back();
  }

}
