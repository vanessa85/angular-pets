import { Injectable } from '@angular/core';

import { Pet } from './models/pet';
import { PETS } from './mock-pets';

import { CategoryService } from './category.service';

@Injectable()
export class PetService {

  constructor(private categoryService: CategoryService) { }

  getPets(): Pet[] {
    return PETS;
  }

  getPet(id): Pet {
    return PETS.find(pet => pet.id === id);
  }

  addPet(pet: Pet) {
    pet.id = PETS.length + 1;
    let category = this.categoryService.getCategoryById(pet.category.id);
    pet.category = category;
    PETS.unshift(pet);
  }

  removePet(pet) {
    let position = PETS.indexOf(pet);
    PETS.splice(position, 1);
  }
  
  updatePet(pet) {
    let category = this.categoryService.getCategoryById(pet.category.id);
    pet.category = category;
  }

}
