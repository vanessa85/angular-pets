import { Pet } from './models/pet';
import { Category } from './models/category';

export const PETS: Pet[] = [
  { id: 11, category: { id: 1, name: 'Category 1' } , name: 'test 1', photoUrls: ['https://www.pethero.com/wp-content/uploads/2018/01/doug-the-pug-2-150x150.jpg'], tags: ['pet', 'animal']},
  { id: 12, category: { id: 2, name: 'Category 2' }, name: 'test 2', photoUrls: ['https://www.pethero.com/wp-content/uploads/2018/01/doug-the-pug-2-150x150.jpg'], tags: ['pet', 'animal'] },
  { id: 13, category: { id: 3, name: 'Category 3' }, name: 'test 3', photoUrls: ['https://www.pethero.com/wp-content/uploads/2018/01/doug-the-pug-2-150x150.jpg'], tags: ['pet', 'animal'] },
  { id: 14, category: { id: 4, name: 'Category 4' }, name: 'test 4', photoUrls: ['https://www.pethero.com/wp-content/uploads/2018/01/doug-the-pug-2-150x150.jpg'], tags: ['pet', 'animal'] }
];