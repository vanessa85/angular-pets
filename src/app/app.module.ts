import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryService } from './category.service';
import { AppRoutingModule } from './/app-routing.module';
import { PetsComponent } from './pets/pets.component';
import { PetService } from './pet.service';
import { PetDetailComponent } from './pet-detail/pet-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    PetsComponent,
    PetDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [CategoryService, PetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
