import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Category } from './models/category';

@Injectable()
export class CategoryService {

  constructor(private http: HttpClient) { }

  // getCategories(): Observable<any> {
  //  return this.http.get('./assets/mockups/categories.json');
  // }

  getCategories() : Category[] {
    return [
      { id: 1, name: 'Category 1' },
      { id: 2, name: 'Category 2' },
      { id: 3, name: 'Category 3' },
      { id: 4, name: 'Category 4' },
    ];
  }

  getCategoryById(id) {
    return this.getCategories().find(category => category.id == id);
  }

}
